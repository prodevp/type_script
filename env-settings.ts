import * as types from '../types';

// 环境配置（Environment Settings）
export class EnvSettings {
  public runPeriod: [types.DateStr, types.DateStr];
  public outputPeriod: [types.DateStr, types.DateStr];
  public modelingScope: ModelingScope;
  public dataProcessingMode: DataProcessingMode;
  public truckLoadingPolicy: TruckLoadingPolicy;
  public truckDeparturePolicy: TruckDeparturePolicy;
  public additionalTruckPolicy: AdditionalTruckPolicy;
  public pendingCheckGate: types.TimeStr;
  public pendingCheckWindow: number; // number of hours
  public baseWeight?: types.ValueUnitPair<boolean, types.WeightUnitKeys>;
  public baseVolume: types.ValueUnitPair<boolean, types.VolumeUnitKeys>;
  public baseCounting: types.ValueUnitPair<boolean, types.CountingUnitKeys>;
  public routePolicy: RoutePolicy;
  public geographicScope: types.GeographicScope;
  public administrationUnit: types.AdministrationUnit;
}

export interface IformItemLayout {
  labelCol: {
    xs: object;
    sm: object;
  };
  wrapperCol: {
    xs: object;
    sm: object;
    md: object;
  };
}

export interface SubmitFormLayouts {
  wrapperCols: {
    xs: object;
    sm: object;
  };
}

export enum ModelingScope {
  CustomerBased = 'customer',
  DCBased = 'dc',
}

export enum DataProcessingMode {
  ByBill = 'by-bill',
  ByPackage = 'by-package',
}

export enum TruckLoadingPolicy {
  OneByOneLoad = 'one-by-one',
  InstantLoad = 'instant',
  ObligatoryLoad = 'obligatory',
}

export enum TruckDeparturePolicy {
  FullTruckload = 'full-truckload',
  LessThanTruckload = 'less-than-truckload',
}

export enum AdditionalTruckPolicy {
  NoAdditionalTruck = 'no-additional-truck',
  AllLoaded = 'all-loaded',
  PendingOnly = 'pending-only',
}

export enum RoutePolicy {
  SingleRoute = 'single',
  SubstituteRoute = 'substitute',
  ProbabilityRoute = 'probability',
}
